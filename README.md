# Synopsis

Examples 
* [c programs ( files with c extension): *.c](./src)
* [processing  sketchbook (Drawing app), files with pde extension = *.pde](./src)


for drawing Mandelbrot set as described in [wiki by A Cheritat](https://www.math.univ-toulouse.fr/~cheritat/wiki-draw/index.php/Mandelbrot_set)

The purpose is educational so:
- optimisation is skipped ( acces to disc for every pixel is slow )
- code should be not fast but self-explanatory, easy to read
- multiplatform
- one file program

# similar projects
* Mandelbrot notebook : [how to write a book about the Mandelbrot set by Claude Heiland-Allen](https://code.mathr.co.uk/mandelbrot-book) and [blog](http://mathr.co.uk/blog/2014-03-06_mandelbrot_notebook.html)
  * [c ( not diff version)](https://gitlab.com/adammajewski/my-book) and [pdf](https://gitlab.com/adammajewski/git-book-pdf)
  * mandelbrot-book/book : [one file c programs](https://gitlab.com/adammajewski/mandelbrot-book_book)
* [Linas' art gallery](http://linas.org/art-gallery/index.html) and [it's clone](https://gitlab.com/adammajewski/LinasArtGallery_MandelbrotSet)


# Algorithms
* BET = Binary Escape Time
* IET = Integer Escape Time = LSM = Level Set Method
  * black and white only
  * IETPM = IET Palette gradient
  * IETGM = IET Gray gradient
* Interior detection methods ( when derivative vanishes)
* CPM = Continous Potential Method ( complex potential )
* DEM = Distance Estimation Method 
  * Milnor = demmm
    * Variation: (partial) antialias effect without oversampling
    * Variation: using the distance estimator to color the outside
  * Henriksen's
* gamma-correct downscale
* SAM = Stripe Average Method = SAC = SA Coloring 
* Normal map effect : shading via a normal map 
  * using potential
  * using distance estimation
* mix of the algorithms : the radial stands + shading via a normal map


here extra m means Mandelbrot set, like in DEM/M 

## BET
```c
 int iterate(double complex C , int iMax)
  {
   int i;
   double complex Z= 0.0; // initial value for iteration Z0
   
   for(i=0;i<iMax;i++)
    {
      Z=Z*Z+C; // https://stackoverflow.com/questions/6418807/how-to-work-with-complex-numbers-in-c
      if(cabs(Z)>EscapeRadius) break;
    }
   return i; 
 }
``` 

Coloring:
```c
   
  // boolean or binary escape time 
  if (i==IterationMax)
    { /*  interior of Mandelbrot set = black  */
      color[0]=0;
      color[1]=0;
      color[2]=0;
    }
  else 
    { /* exterior of Mandelbrot set = white  */
      color[0]=255; /* Red*/
      color[1]=255;  /* Green */ 
      color[2]=255;/* Blue */
    };
``` 

![BET = Binary Escape Time ](./images/betm.png "BET")

## IET or LSM

black and white only: here only coloring is different:

```c
 // boolean or binary escape time 
  if (i==IterationMax)
    { /*  interior of Mandelbrot set = black  */
      color[0]=0;
      color[1]=0;
      color[2]=0; 
    }
  else 
     //        exterior of Mandelbrot set = LSM using only 2 colors:  B&W
      //  https://commons.wikimedia.org/wiki/File:Mandel_lsm_bw.jpg
      
   if ((i % 2)==0) 
     { color[0]=0; // Red
       color[1]=0;  // Green 
       color[2]=0;// Blue 
     }
   else 
     {
      color[0]=255; /* Red*/
      color[1]=255;  /* Green */ 
      color[2]=255;/* Blue */
     };
```



![IET = Integer Escape Time = LSM = Level Set Method](./images/ietm.png "IET")

gray colors - coloring of exterior is different:
```c
 // boolean or binary escape time 
  if (i==IterationMax)
    { /*  interior of Mandelbrot set = black  */
      color[0]=0;
      color[1]=0;
      color[2]=0; 
    }
  else // exterior of Mandelbrot set = LSM using gray gradient 
        
    { 
     g = (unsigned char)(255*i/IterationMax); // map to [0 : 255 ] range
     g = 30.0 * g ; // repetition of gradient and difference between level sets 
     g = g % 255 ;  //  clipping to [0;255] range 
     g = 255 - g ; // inverse 
     // the same g for all = gray 
     color[0]=g;  /* Red*/
     color[1]=g;  /* Green */ 
     color[2]=g;  /* Blue */
                            
     };
```

![IET = Integer Escape Time = LSM = Level Set Method - gray colors](./images/ietgm.png "IET")

fractint default palette
```c
 // boolean or binary escape time 
  if (i==IterationMax)
    { /*  interior of Mandelbrot set = black  */
      color[0]=0;
      color[1]=0;
      color[2]=0; 
    }
  else // exterior of Mandelbrot set = LSM using palette gradient gradient 
        
    { 
     
     p = i; // (i+1);
     p = p % 255; // in cas IterationMax>255 then map to [0, 255]
     p = 3*p; // map to [0 : length(paleette) = 3*255 ] range, 
     //start from second color : p=1 
     
     color[0]=palette[p];  /* Red*/
     color[1]=palette[p+1];  /* Green */ 
     color[2]=palette[p+2];  /* Blue */
                            
     };
``` 
 


![IET = Integer Escape Time = LSM = Level Set Method- fractint default palette](./images/ietpm.png "IET")

## Interior detection method
time with detection versus without detection is : 0.383s versus 8.692s so it is 23 times faster !!!!

```c
// gives last iterate = escape time
// output 0< i < iMax
 int iterate(double complex C , int iMax)
  {
   int i=0;
   double complex Z= C; // initial value for iteration Z0
   complex double D = 1.0; // derivative with respect to z 
   
   for(i=0;i<iMax;i++)
    { if(cabs(Z)>EscapeRadius) break;     // exterior
      if(cabs(D)<AttractionRadius) break; // interior
      D = 2.0*D*Z;
      Z=Z*Z+C; // complex quadratic polynomial
      
    }
   return i; 
 }
``` 
 
![Interior detection methods ](./images/m_int.png "M_int")

## CPM

```c
// potential(c,n) = log(abs(zn))/2^n
// https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/MandelbrotSetExterior#Real_potential_.3D_CPM.2FM
double GivePotential(double complex C , int iMax)
  {
   int i=0; // iteration 
   int power=1; // 2^n computed by iteration
   double complex Z= 0.0; // initial value for iteration Z0
   double potential = FP_ZERO; // inside 
   double z_radius;
   
   
   
   for(i=0;i<iMax;i++)
    { 
      Z=Z*Z+C; // https://stackoverflow.com/questions/6418807/how-to-work-with-complex-numbers-in-c
      power = power*2; 
      z_radius = cabs(Z);
      
      if(z_radius > EscapeRadius) { // exterior of M set
       potential = log(z_radius)/power;
       break;
      
      }
    }
   return potential; 
 }
```

and coloring: 
```c
  // 
  if (potential == FP_ZERO)
    { /*  interior of Mandelbrot set = inside_color = dark bordeaux red 120,15,33 */
      color[0]=191; // M_waves
      color[1]=191;
      color[2]=191; 
    }
  else // exterior of Mandelbrot set = CPM 
        
    { 
     
     x = log(potential)/K;
     b = 255* (1+cos(2.0*M_PI*x))/2.0;
     //b = (unsigned char) (255 - 255*potential );
     
     color[0]= b;  /* Red*/
     color[1]= b;  /* Green */ 
     color[2]= b;  /* Blue */
                            
     };
```

![CPM = Continous Potential Method ( complex potential ) ](./images/cpmm.png "CPM")

## DEM

Distance Estimation Method for Mandelbrots set = DEM/M 
### Milnor version = demmm

"we have an estimate dn for the distance from c to M, where n is the first index for which |zn|>ER. 
...
we compared this estimate to a fixed size s and set the pixel 
* black if dn < s 
* white otherwise"



![DEM ](./images/demmm.png "DEM") 

```c
double Give_D(double complex C , int iMax)
{
  int i=0; // iteration 
   
   
  double complex Z= 0.0; // initial value for iteration Z0
  double R; // =radius = cabs(Z)
  double D; 
  double complex dC = 0; // derivative
  double de; // = 2 * z * log(cabs(z)) / dc;
   
    
  // iteration = computing the orbit
  for(i=0;i<iMax;i++)
    { 
    
      dC = 2 * Z * dC + 1; 
      Z=Z*Z+C; // https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/qpolynomials
      
            
      R = cabs(Z);
      if(R > EscapeRadius) break; // exterior of M set
   
      
    } // for(i=0
   
   
  if (i == iMax) D = -1.0; // interior 
    else { // exterior
      de = 2.0 * R * log(R) / cabs(dC) ; // 
    
      if (de < MinBoundaryWidth) D = FP_ZERO; //  boundary
         else  D = 1.0; // exterior
    }
    
  return D; 
}
 
```

and coloring method:

```c
int compute_color(complex double c, unsigned char color[3]){
 
  double d ;
  unsigned char b;
   
  // compute 
  d = Give_D( c, IterationMax);
     
  // 
  if (d < 0.0)
     /*  interior of Mandelbrot set = inside_color =  */
      b = 191; 
    
  else // exterior and boundary
     { 
       if (d == FP_ZERO) b = 255; // boundary 
          else b = 0; // exterior
     };
    
 // gray gradient
 color[0]= b;  /* Red*/
 color[1]= b;  /* Green */ 
 color[2]= b;  /* Blue */
 
 
  return 0;
}
```
#### Variation: (partial) antialias effect without oversampling

"... we have an estimate dn for the distance from c to M, ...
In the previous algorithm we compared this estimate to a fixed size s and set the pixel black if dn < s or white otherwise. 
The idea here is to use the quotient dn/s as an interpolation factor from black to white.
... 
This is a very simple way to get (partial) antialias without oversampling."


#### Variation: using the distance estimator to color the outside

"color = black if close to 0, otherwise alternating bands of white and violet, according to the parity of the integer part of log(d)/constant for some constant"



## gamma-correct downscale
>>>
... a nice experiment: below we chose a zoom in a place where M is very dense. The picture would be mostly black. We then set the thickness factor to a small vallue (I think it was 0.03) and computed an image of 8000x8000 pixels, that we downscaled to 800x800. We paid extra attention at performing a gamma-correct downscale. The result is quite interesting.
>>>

![M2_sg.png](./images/M2_sg.png "Original image by Arnoud Cheritat")  


One can check it with Gimp
* open image
* [Gimp/Tools/Color tools/Levels](https://docs.gimp.org/2.8/en/gimp-tool-levels.html)
* below Input Levels there are 3 triangle sliders. Choose middle triangle (grey) for midtones. It is for gamma adjustment. 
* Going to the left, to the black, makes the image lighter (more colored / more opaque) . Going to the right, to the white, makes the image darker (less colored / more transparent).



[How to find precise location ?](https://fractalforums.org/programming/11/gamma-correction-and-location-of-zoom-image/3186)


Answer by Claude :   

>>>
The outer light filaments have 3-way spirals.  So the first angle is {1,2}/3.
The inner light filaments have 18-way spirals.  So the next angle is {1,5,7,11,13,17}/18.
There are dark embedded Julia sets visible in the light filaments, so the zoom must have passed an island near its seahorse valley (because of the shape of the EJSs).
There is no additional shape stacking, so the zoom passed at most one island.
The thick filaments are tuned by an island.
The outer thick filaments have 2-way spirals.  So the first angle from the island is 1/2.
The inner thick filaments have 11-way spirals.  So the second angle from the island is {1..10}/11.
The largest visible island is highly distorted, which occurs at the lowest period island in the widest spoke attached to the principal hub.
So the angled internal address of this distorted island detuned relative to its parent is 1 1/2 2 a/11 22 1/2 24.
And the angled internal address of the parent island begins 1 b/3 3 c/18 54 1/2 ....

Attached is not quite right, probably incorrect choice of numerators and the missing ... in the parent address

Location for MuET for FragM:

Zoom = 123456789 Logarithmic
Center = -0.05794121488070425808200419,0.8132682994364264091896378
>>>

So:  
* center = -0.05794121488070425808200419e+00 0.8132682994364264091896378e+00  
* radius ( 1/zoom) = 8.100000073710000e-09




See also:
* [FF : technical-challenge the lichtenberg figure](https://fractalforums.org/fractal-image-of-the-month/70/technical-challenge-discussion-the-lichtenberg-figure/3639/msg22657#msg22657)

## SAM or SAC

```c
// the addend function
// input : complex number z
// output : double number t 
double Give_t(double complex z){

  return 0.5+0.5*sin(s*carg(z));

}

/*
  input :
  - complex number
  - intege
  output = average or other estimators, like distance or interior
 
*/
double Give_Arg(double complex C , int iMax)
{
  int i=0; // iteration 
   
   
  double complex Z= 0.0; // initial value for iteration Z0
  double A = 0.0; // A(n)
  double prevA = 0.0; // A(n-1)
  double R; // =radius = cabs(Z)
  double d; // smooth iteration count
  double complex dC = 0; // derivative
  double de; // = 2 * z * log(cabs(z)) / dc;
   
    
  // iteration = computing the orbit
  for(i=0;i<iMax;i++)
    { 
    
      dC = 2 * Z * dC + 1; 
      Z=Z*Z+C; // https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/qpolynomials
      
      if (i>i_skip) A += Give_t(Z); // 
      
      R = cabs(Z);
      if(R > EscapeRadius) break; // exterior of M set
   
      prevA = A; // save value for interpolation
        
    } // for(i=0
   
   
  if (i == iMax) 
    A = -1.0; // interior 
  else { // exterior
    de = 2 * R * log(R) / cabs(dC);
    if (de < PixelWidth) A = FP_ZERO; //  boundary
    else {
      // computing interpolated average
      A /= (i - i_skip) ; // A(n)
      prevA /= (i - i_skip - 1) ; // A(n-1) 
      // smooth iteration count
      d = i + 1 + log(lnER/log(R))/M_LN2;
      d = d - (int)d; // only fractional part = interpolation coefficient
      // linear interpolation
      A = d*A + (1.0-d)*prevA;
     } 
  }
    
  return A;
}
```

and coloring: 
```c
  // compute 
  arg = Give_Arg( c, IterationMax);
     
  // 
  if (arg < 0.0)
     /*  interior of Mandelbrot set = inside_color =  */
      b = 0; 
    
  else // exterior and boundary 
     { 
     
     if (arg == FP_ZERO) b= 255; // boundary 
       else b = (unsigned char) (255 - 255*arg );
      
      
    };
    
    
    // gray gradient
      color[0]= b;  /* Red*/
      color[1]= b;  /* Green */ 
      color[2]= b;  /* Blue */
  return 0;
}
``` 

![Stripe Average Coloring (or Method) + DEM ](./images/samm.png "SAM")

## Normal map

"our normal map is not realistic in the sense that there can be no height field whose normals are in these directions (by lack of integrability)"


Steps
* choose 
  * R big enough, for example R = 100 ; do not take R too small 
  * a constant vector indicating the direction of the light = incoming light 3D vector = normalized light-direction = (v.re, v.im, h2)
* check every point c if it is on the outside of M. If yes then:
  * compute 3D vector: (u.re, u.im, 1) which is normal to the equipotential at point c 
  * compute the reflection t by taking the dot product of normal vector and  light vector
  * compute color color = linear_interpolation(black,white,t)
 



### using complex potential and Lambert reflection (Illumination model or shader )

Mande with help of Claude Heiland-Allen : http://mathr.co.uk/blog/


![Normal map ](./images/normal.png "normal")


### Variant

![Normal map variant ](./images/normal_d.png "normal")
 
  
# Code Example
Code uses complex numbers so c code is short and looks like pseudocode:

```c
int iterate(double complex C , int iMax)
  {
   int i;
   double complex Z= 0.0; // initial value for iteration Z0
   
   for(i=0;i<iMax;i++)
    {
      Z=Z*Z+C; // https://stackoverflow.com/questions/6418807/how-to-work-with-complex-numbers-in-c
      if(cabs(Z)>EscapeRadius) break;
    }
   return i; 
 }
``` 


# Motivation

Show how to implement computing and coloring algorithms 







## Contributors

are wellcome 


  
## License

A short snippet describing the license (MIT, Apache, etc.)


# technical notes
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)

## API Reference

simple one file c programs which 
- do not need any extra libraries 
- can be run from console
- compiled with gcc 
- multiplatform

How to compile and run is described in the comments of c files


## Git
```git
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot_wiki_ACh.git
git add html
git commit -m "html"
git push -u origin master
```



```
  git clone git@gitlab.com:adammajewski/mandelbrot_wiki_ACh.git
```


local repo : ~/c/mandel/wiki_ACh
