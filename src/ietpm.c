
/* 
   c program:
   ietpm = Integer Escape Time - Palette gradient  Mandelbrot set 
   = LSM/M = Level Set Method for Mandelbrot set
   
   
   https://en.wikibooks.org/wiki/Color_Theory/Color_gradient#Palette
   
   --------------------------------
   1. draws Mandelbrot set for Fc(z)=z*z +c
   using Mandelbrot algorithm ( boolean escape time )
   -------------------------------         
   2. technique of creating ppm file is  based on the code of Claudio Rocchini
   http://en.wikipedia.org/wiki/Image:Color_complex_plot.jpg
   create 24 bit color graphic file ,  portable pixmap file = PPM 
   see http://en.wikipedia.org/wiki/Portable_pixmap
   to see the file use external application ( graphic viewer)
-----
 it is example  for : 
 https://www.math.univ-toulouse.fr/~cheritat/wiki-draw/index.php/Mandelbrot_set
 
 -------------
 compile : 

 
 
   gcc ietpm.c -lm -Wall
 
 
   ./a.out
   
   
   -------- git --------
   
   
   cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot_wiki_ACh.git
git add ietpm.c
git commit -m "ietm = Integer Escape Time - Palette gradient Mandelbrot set 
   = LSM/M = Level Set Method for Mandelbrot set"
git push -u origin master



 
 
 
*/
#include <stdio.h>
#include <math.h>
#include <complex.h> // https://stackoverflow.com/questions/6418807/how-to-work-with-complex-numbers-in-c
 
 

 
 
 
 
/* screen ( integer) coordinate */
int iX,iY;
const int iXmax = 8000; 
const int iYmax = 6000;
/* world ( double) coordinate = parameter plane*/
double Cx,Cy;
const double CxMin=-2.5;
const double CxMax=1.5;
const double CyMin=-1.5;
const double CyMax=1.5;
/* */
double PixelWidth; //=(CxMax-CxMin)/iXmax;
double PixelHeight; // =(CyMax-CyMin)/iYmax;
/* color component ( R or G or B) is coded from 0 to 255 */
/* it is 24 bit color RGB file */
const int MaxColorComponentValue=255; 
FILE * fp;
char *filename="ietpm.ppm"; 
char *comment="# ";/* comment should start with # */
        
static unsigned char color[3]; // 24-bit rgb color

// 1D array = palette 
// default.map from fractint = VGA
unsigned char palette[3*256]= 
{0,0,0, // first color p=0
0,0,188, // second color p=1 is blue
0,188,0, // green
0,188,188,
188,0,0,
168,0,168,
168,84,0,
168,168,
168,84,84,84,84,84,252,84,252,84,84,252,252,252,84,84,252,84,252,252,252,84,252,252,252,0,0,0,20,20,20,32,32,32,44,44,44,56,56,56,68,68,68,80,80,80,96,96,96,112,112,112,128,128,128,144,144,144,160,160,160,180,180,180,200,200,200,224,224,224,252,252,252,0,0,252,64,0,252,124,0,252,188,0,252,252,0,252,252,0,188,252,0,124,252,0,64,252,0,0,252,64,0,252,124,0,252,188,0,252,252,0,188,252,0,124,252,0,64,252,0,0,252,0,0,252,64,0,252,124,0,252,188,0,252,252,0,188,252,0,124,252,0,64,252,124,124,252,156,124,252,188,124,252,220,124,252,252,124,252,252,124,220,252,124,188,252,124,156,252,124,124,252,156,124,252,188,124,252,220,124,252,252,124,220,252,124,188,252,124,156,252,124,124,252,124,124,252,156,124,252,188,124,252,220,124,252,252,124,220,252,124,188,252,124,156,252,180,180,252,196,180,252,216,180,252,232,180,252,252,180,252,252,180,232,252,180,216,252,180,196,252,180,180,252,196,180,252,216,180,252,232,180,252,252,180,232,252,180,216,252,180,196,252,180,180,252,180,180,252,196,180,252,216,180,252,232,180,252,252,180,232,252,180,216,252,180,196,252,0,0,112,28,0,112,56,0,112,84,0,112,112,0,112,112,0,84,112,0,56,112,0,28,112,0,0,112,28,0,112,56,0,112,84,0,112,112,0,84,112,0,56,112,0,28,112,0,0,112,0,0,112,28,0,112,56,0,112,84,0,112,112,0,84,112,0,56,112,0,28,112,56,56,112,68,56,112,84,56,112,96,56,112,112,56,112,112,56,96,112,56,84,112,56,68,112,56,56,112,68,56,112,84,56,112,96,56,112,112,56,96,112,56,84,112,56,68,112,56,56,112,56,56,112,68,56,112,84,56,112,96,56,112,112,56,96,112,56,84,112,56,68,112,80,80,112,88,80,112,96,80,112,104,80,112,112,80,112,112,80,104,112,80,96,112,80,88,112,80,80,112,88,80,112,96,80,112,104,80,112,112,80,104,112,80,96,112,80,88,112,80,80,112,80,80,112,88,80,112,96,80,112,104,80,112,112,80,104,112,80,96,112,80,88,112,0,0,64,16,0,64,32,0,64,48,0,64,64,0,64,64,0,48,64,0,32,64,0,16,64,0,0,64,16,0,64,32,0,64,48,0,64,64,0,48,64,0,32,64,0,16,64,0,0,64,0,0,64,16,0,64,32,0,64,48,0,64,64,0,48,64,0,32,64,0,16,64,32,32,64,40,32,64,48,32,64,56,32,64,64,32,64,64,32,56,64,32,48,64,32,40,64,32,32,64,40,32,64,48,32,64,56,32,64,64,32,56,64,32,48,64,32,40,64,32,32,64,32,32,64,40,
32,64,48,32,64,56,32,64,64,32,56,64,32,48,64,32,40,64,44,44,64,48,44,64,52,44,64,60,44,64,64,44,64,64,44,60,64,44,52,64,44,48,64,44,44,64,48,44,64,52,44,64,60,44,64,64,44,60,64,44,52,64,44,48,64,44,44,64,44,44,64,48,44,64,52,44,64,60,44,64,64,44,60,64,44,52,64,44,48,64,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

/* Z=Zx+Zy*i  ;   Z0 = 0 */
//double Zx, Zy;
//double Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
/*  */
int Iteration;
const int IterationMax=150;
/* bail-out value , radius of circle ;  */
const double EscapeRadius=2;
//double ER2=EscapeRadius*EscapeRadius;
        
        
        
        
 double complex give_c(int iX, int iY){
  double Cx,Cy;
  Cy=CyMin + iY*PixelHeight;
  if (fabs(Cy)< PixelHeight/2) Cy=0.0; /* Main antenna */
  Cx=CxMin + iX*PixelWidth;
   
  return Cx+Cy*I;
 
 
}
 

// print 24-bit color from 1D array = palette 
void test_palette(unsigned char p){
printf("p = %d  color :  R= %d G= %d B = %d  \n", p, palette[p*3], palette[p*3+1], palette[p*3+2]);


}


// gives last iterate = escape time
// output 0< i < iMax
 int iterate(double complex C , int iMax)
  {
   int i=0;
   double complex Z= 0.0; // initial value for iteration Z0
   
   for(i=0;i<iMax;i++)
    { if(cabs(Z)>EscapeRadius) break; 
      Z=Z*Z+C; // https://stackoverflow.com/questions/6418807/how-to-work-with-complex-numbers-in-c
      
    }
   return i; 
 }
 
 
 
 
 
int compute_color(complex double c, unsigned char color[3]){
 
   int i; // last iteration
   int p; // index of palette   
   
   
   // compute escape time = last iteration:  1<i <= IterationMax
   i = iterate( c, IterationMax);
  
    
  // boolean or binary escape time 
  if (i==IterationMax)
    { /*  interior of Mandelbrot set = black  */
      color[0]=0;
      color[1]=0;
      color[2]=0;                           
    }
  else // exterior of Mandelbrot set = LSM using palette gradient gradient 
        
    { 
     
     p = i; // (i+1);
     p = p % 255; // in cas IterationMax>255 then map to [0, 255]
     p = 3*p; // map to [0 : length(paleette) = 3*255 ] range, 
     //start from second color : p=1   
     
     color[0]=palette[p];  /* Red*/
     color[1]=palette[p+1];  /* Green */ 
     color[2]=palette[p+2];  /* Blue */
                            
     };
 
   
  return 0;
}
 
 
 
 void setup(){
 
  //
  PixelWidth=(CxMax-CxMin)/iXmax;
  PixelHeight=(CyMax-CyMin)/iYmax;
        
         
  /*create new file,give it a name and open it in binary mode  */
  fp= fopen(filename,"wb"); /* b -  binary mode */
  /*write ASCII header to the file*/
  fprintf(fp,"P6\n %s\n %d\n %d\n %d\n",comment,iXmax,iYmax,MaxColorComponentValue);
 
 }
 





void info(){




 double distortion;
 // widt/height
 double PixelsAspectRatio = (double)iXmax/iYmax;  // https://en.wikipedia.org/wiki/Aspect_ratio_(image) 
 double WorldAspectRatio = (CxMax-CxMin)/(CyMax-CyMin);

 distortion = PixelsAspectRatio - WorldAspectRatio;
 printf("distortion = %.16f ( it should be zero !)\n", distortion );

 // file  
 printf("file %s saved \n", filename);

 // palette
 test_palette(0);
 test_palette(1);
 test_palette(2);
 test_palette(3);
 
 // iteration
 complex double c = 3.0;
 complex double z= 0.0;
 z = z*z+c;
 if(cabs(z)>EscapeRadius) 
    printf("cabs(%f)>EscapeRadius) = escapes \n", creal(z));
    else printf("cabs(%f)<EscapeRadius) = not escapes = bounded \n", creal(z));
 
 printf("for IterationMax = %d last i = %d \n",0, iterate(z,0));
 printf("for IterationMax = %d last i = %d \n",1, iterate(z,1));
 printf("for IterationMax = %d last i = %d \n",2, iterate(z,2));
 //
 c = 1.9;
 z = 0.0;
 z = z*z+c;
 if(cabs(z)>EscapeRadius) 
   printf("cabs(%f)>EscapeRadius) = escapes \n", creal(z));
   else printf("cabs(%f)<EscapeRadius) = not escapes = bounded \n", creal(z));
  
 printf("for IterationMax = %d last i = %d \n",0, iterate(z,0));
 printf("for IterationMax = %d last i = %d \n",1, iterate(z,1));
 printf("for IterationMax = %d last i = %d \n",2, iterate(z,2));
}
 













 
 void close(){
 
 
 
 fclose(fp);
 info(); 
 
 
 
 
 }
 
 
 
 
 
// ************************************* main ************************* 
int main()
{
        
  complex double c;
        
        
 
  setup();      
        
        
  printf(" render = compute and write image data bytes to the file \n");
 
  for(iY=0;iY<iYmax;iY++)
    for(iX=0;iX<iXmax;iX++)
      { // compute pixel coordinate        
	c = give_c(iX, iY);  
	/* compute  pixel color (24 bit = 3 bytes) */
	compute_color(c,color);         
	/*write color to the file*/
	fwrite(color,1,3,fp);
      }
        
  
  
  close();
  
         
  return 0;
}
