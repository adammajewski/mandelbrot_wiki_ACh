
/* 
   c program: console, 1-file
   samm.c
   
   algorithms:
   - escape time
   - DEM/M = distance estimation, Milnor version 
   
   
   "we have an estimate dn for the distance from z0 to M, 
   where n is the first index for which |zn|>ER. 
   ...
   we compared this estimate to a fixed size s and set the pixel 
   * black if dn<s 
   * white otherwise" A Cheritat
   
   
  
   
   --------------------------------
   1. draws Mandelbrot set for complex quadratic polynomial 
   Fc(z)=z*z +c
   using samm = Stripe Average Method/Coloring  
   -------------------------------         
   2. technique of creating ppm file is  based on the code of Claudio Rocchini
   http://en.wikipedia.org/wiki/Image:Color_complex_plot.jpg
   create 24 bit color graphic file ,  portable pixmap file = PPM 
   see http://en.wikipedia.org/wiki/Portable_pixmap
   to see the file use external application ( graphic viewer)
   -----
   it is example  for : 
   https://www.math.univ-toulouse.fr/~cheritat/wiki-draw/index.php/Mandelbrot_set
 
   -------------
   compile : 

 
 
   gcc demm_milnor.c -lm -Wall 
 
 
   ./a.out
   
   convert samm.ppm -resize 800x800 samm.png

   -------- git --------
   
   
   cd existing_folder
   git init
   git remote add origin git@gitlab.com:adammajewski/mandelbrot_wiki_ACh.git
   git add samm.c
   git commit -m "samm + linear interpolation "
   git push -u origin master



 
 
 
*/
#include <stdio.h>
#include <math.h>
#include <complex.h> // https://stackoverflow.com/questions/6418807/how-to-work-with-complex-numbers-in-c

 


 
/* screen ( integer) coordinate */
int iX,iY;
const int iXmax = 2400; 
const int iYmax = 2401; // +1 for the main antenna, convert demmm1.ppm -resize 800x800 demmm.png


/* world ( double) coordinate = parameter plane*/
double Cx,Cy;

// c = -0.355298979690605  +1.178016112830515 i    period = 0
// c = 0.213487557597331  +0.604701020309395 i    period = 0
const double CxMin=-2.25;
const double CxMax=0.75;
const double CyMin=1.5;
const double CyMax= -1.5;
/* */
double PixelWidth; //=(CxMax-CxMin)/iXmax;
double PixelHeight; // =(CyMax-CyMin)/iYmax;

/* color component ( R or G or B) is coded from 0 to 255 */
/* it is 24 bit color RGB file */
const int MaxColorComponentValue=255; 
FILE * fp;
char *filename="demm2400.ppm"; // 
char *comment="# ";/* comment should start with # */
        
static unsigned char color[3]; // 24-bit rgb color


const int IterationMax=2500; //  N in wiki 


/* bail-out value for the bailout test for exaping points
   radius of circle centered ad the origin, exterior of such circle is a target set  */
const double EscapeRadius=1000; // = ER big !!!!

double thickness = 1.50;// multiplier 
double MinBoundaryWidth; //       
        
        
        
double complex give_c(int iX, int iY){
  double Cx,Cy;
  
  Cy=CyMax - iY*PixelHeight; // reverse Y axis
  //if (fabs(Cy)<PixelHeight) Cy = 0.0; 
  Cx=CxMin + iX*PixelWidth;
   
  return Cx+Cy*I;
 
 
}
 




/*
  input :
  - complex number
  - intege
  output =  estimator dn
   
*/
double Give_D(double complex C , int iMax)
{
  int i=0; // iteration 
   
   
  double complex Z= 0.0; // initial value for iteration Z0
  double R; // =radius = cabs(Z)
  double D; // output of the function
  double complex dC = 0; // derivative with respect to c 
  // dn in A Cheritat notation
  double de; // = 2 * z * log(cabs(z)) / dc =  estimated distance to the Mandelbrot set
   
    
  // iteration = computing the orbit
  for(i=0;i<iMax;i++)
    { 
    
      dC = 2 * Z * dC + 1; 
      Z=Z*Z+C; // https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/qpolynomials
      
            
      R = cabs(Z);
      if(R > EscapeRadius) break; // exterior of M set
   
      
    } // for(i=0
   
   
  if (i == iMax) D = -1.0; // interior 
    else { // exterior
      de = 2.0 * R * log(R) / cabs(dC) ; // 
    
      if (de < MinBoundaryWidth) D = FP_ZERO; //  boundary
         else  D = 1.0; // exterior
    }
    
  return D;  
}
 

/* 
 
 input = complex number
 output 
  - color array as Formal parameters
  - int = return code
*/
int compute_color(complex double c, unsigned char color[3]){
 
  double d ;
  unsigned char b;
   
  // compute 
  d = Give_D( c, IterationMax);
     
  // 
  if (d < 0.0)
     /*  interior of Mandelbrot set = inside_color =  */
      b = 191;                          
    
  else // exterior and boundary
     {      
       if (d == FP_ZERO) b = 255; // boundary     
          else b = 0; // exterior
     };
    
 // gray gradient
 color[0]= b;  /* Red*/
 color[1]= b;  /* Green */ 
 color[2]= b;  /* Blue */
 
 
  return 0;
}
 
 
 
void setup(){
 
  //
  PixelWidth=(CxMax-CxMin)/iXmax;
  PixelHeight=(CyMax-CyMin)/iYmax;
  //
  MinBoundaryWidth = thickness*PixelWidth;
  
        
         
  /*create new file,give it a name and open it in binary mode  */
  fp= fopen(filename,"wb"); /* b -  binary mode */
  /*write ASCII header to the file*/
  fprintf(fp,"P6\n %s\n %d\n %d\n %d\n",comment,iXmax,iYmax,MaxColorComponentValue);
 
}
 



void info(){

  double distortion;
  // widt/height
  double PixelsAspectRatio = (double)iXmax/iYmax;  // https://en.wikipedia.org/wiki/Aspect_ratio_(image) 
  double WorldAspectRatio = (CxMax-CxMin)/(CyMax-CyMin);
  printf("PixelsAspectRatio = %.16f \n", PixelsAspectRatio );
  printf("WorldAspectRatio = %.16f \n", WorldAspectRatio );
  distortion = PixelsAspectRatio - WorldAspectRatio;
  printf("distortion = %.16f ( it should be zero !)\n", distortion );
  printf("\n");
  printf("bailut value = Escape Radius = %.0f \n", EscapeRadius);
  printf("IterationMax = %d \n", IterationMax);
  

  // file  
  printf("file %s saved. It is called MilMand_1000iter in  A Cheritat wiki\n", filename);

}
 



 
void close(){
 
  fclose(fp);
  info(); 

}
 
 
 
 
 
// ************************************* main ************************* 
int main()
{
        
  complex double c;
        
        
 
  setup();      
        
        
  printf(" render = compute and write image data bytes to the file \n");
 //#pragma omp parallel for schedule(static) private(iY, iX, c, color)
  for(iY=0;iY<iYmax;iY++)
    for(iX=0;iX<iXmax;iX++)
      { // compute pixel coordinate        
	c = give_c(iX, iY);  
	/* compute  pixel color (24 bit = 3 bytes) */
	compute_color(c,color);         
	/*write color to the file*/
	fwrite(color,1,3,fp);
      }
        
  
  
  close();
  
         
  return 0;
}
